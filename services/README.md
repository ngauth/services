# @ngauth/services

## Overview

`NgAuth` is a suite of Angular modules
comprising a set of auth-related reusable components and services.
The library is mostly frontend/UI, and it uses AWS Cognito for serverless backend.
The `NgAuth - Services` module includes abstract service interfaces
for backend auth service providers.


_(Note: this library is currently in beta,_
_and the APIs may change over time.)_




## Installation

To install this library, run:

```bash
$ npm install --save @ngauth/services @ngauth/core
```

Note that NgAuth uses dependency injection for providing a concrete backend auth service provider.
You will need at least one such implementation (e.g., `@ngauth/cognito`) to be able to use NgAuth.




## Using the library

From your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import the library
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify the library as an import
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once the library is imported, you can use its components
and services in your Angular application.



## License

MIT © [Harry Y](mailto:sidewaybot@gmail.com)
