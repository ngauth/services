import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { AuthConfigCallback } from '@ngauth/core';
import { ICognitoAuthConfig, DefaultCognitoAuthConf } from '@ngauth/core';


@Injectable()
export class AuthConfigManager
{
  constructor(private authConf: DefaultCognitoAuthConf) {
  }

  // callbacks for auth-config loaded "event".
  private callbacks: AuthConfigCallback[] = [];

  public addCallback(callback: AuthConfigCallback) {
    this.callbacks.push(callback);
  }

  // Note that this public.
  public callCallbacks() {
    for (let c of this.callbacks) {
      c.authConfigLoaded();
    }
  }


  // tbd:
  public loadConfig(): Observable<ICognitoAuthConfig> {
    // return Observable.create((obs) => {
    //   obs.next(this.authConfig);
    // })
    return this.authConf.loadConfig();
  }

  public triggerLoading(): void {
    this.authConf.loadConfig().subscribe((config) => {
      if (config) {
        if(dl.isLoggable()) dl.log("authConfig successfully Loaded.");
        this.callCallbacks();
      } else {
        // ???
        if(dl.isLoggable()) dl.log("Failed to load authConfig.");
      }
    });
  }

  // getConfig(triggerLoading = false): (ICognitoAuthConfig | null) {
  //   let config = this.authConf.getConfig(triggerLoading);
  //   return config;
  // }
  public getConfig(triggerLoading = false): (ICognitoAuthConfig | null) {
    if (triggerLoading) {
      this.triggerLoading();
    }
    return this.authConf.getConfig();
  }

  public get isLoaded(): boolean {
    return this.authConf.isLoaded;
  }


  // These individual field getters are deprecated.
  // To be deleted

  // get region(): (string | null) {
  //   return this.authConfig.region;
  // }

  // get identityPoolId(): (string | null) {
  //   return this.authConfig.identityPoolId;
  // }

  // get userPoolId(): (string | null) {
  //   return this.authConfig.userPoolId;
  // }

  // get userPoolClientId(): (string | null) {
  //   return this.authConfig.userPoolClientId;
  // }

  // get userIdTableName(): (string | null) {
  //   return this.authConfig.userIdTableName;
  // }

}


