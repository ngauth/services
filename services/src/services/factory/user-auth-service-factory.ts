import { Injectable } from "@angular/core";

import { IUserRegistrationService } from '../core/user-registration-service';
import { IUserLoginService } from '../core/user-login-service';
import { IUserAttributesService } from '../core/user-attributes-service';


// Abstract factory.
@Injectable()
export class UserAuthServiceFactory {

  getUserRegistrationService(): IUserRegistrationService {
    return null;
  }

  getUserLoginService(): IUserLoginService {
    return null;
  }
  getUserAttributesService(): IUserAttributesService {
    return null;
  }

}
