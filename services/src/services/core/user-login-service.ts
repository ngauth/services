import { Observable } from 'rxjs/Observable';

import { LoggedInStatus, CognitoResult, NewPasswordInfo } from '@ngauth/core';


export interface IUserLoginService {
  isAuthenticated(): Observable<LoggedInStatus>;
  authenticate(username: string, password: string): Observable<CognitoResult>;
  startResetPassword(username: string): Observable<CognitoResult>;
  confirmResetPassword(email: string, verificationCode: string, password: string): Observable<CognitoResult>;
  changePassword(newPasswordInfo: NewPasswordInfo): Observable<CognitoResult>;
  logout(): void;    // Does it always work? What happens if the logout fails?
}
