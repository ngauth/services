import { Observable } from 'rxjs/Observable';

export interface IUserAttributesService {
  getAttributes(): Observable<any>;
}
